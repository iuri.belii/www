import os
path = os.path.dirname(os.path.realpath(__file__))
acl = ["TNF_teradici", "TNF_webdav_columbus", "TNF_webdav_taira", "HZ_webdav_gonzo", "TNF_webdav_mori"]
USER_list = os.popen("cat " + path + "/acl_user_based.yml | shyaml keys-0 services | xargs -0 -n 1").read()
for i in USER_list.split("\n")[:-1]:
    list_ACS_user = os.popen("cat " + path + "/acl_user_based.yml | shyaml get-value services." + i).read()  
    for j in list_ACS_user.split("\n")[:-1]:
        for ij in acl:
            if ij in j:
                os.system("echo " + i + " >> ./" + ij + ".list" )
