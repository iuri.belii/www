#!/usr/bin/env python
# -*- coding: utf-8 -*-
import yaml
acl_file = "acl_user_based.yml"
file = open(acl_file, 'r')
data = yaml.safe_load(file)
for user, service in data["services"].items():
  for list in service:
    acl_file = open('./ACL_lists/' + list + '.list', 'a+')
    acl_file.write(user + "\n")
